$(document).ready(function(){
  console.log("Welcome to jQuery.");

  var c = $("#myCanvas");
  var ctx = c[0].getContext("2d");

  // Dibujar rectángulo rojo
  ctx.fillStyle = "#FF0000";
  ctx.fillRect(20, 20, 150, 100);

  // Añadir texto
  ctx.font = "30px Arial";
  ctx.strokeText("Hello World",10,50);

  // Definimos variables para el circulo
  var start = 0;
  var end = 0;
  var radius = 15;

  while (end < 1000) {

    dibujaCirculo(ctx, start, end, radius);

    start = start + 30;
    end = end + 10;
  }


 // Función que dibuja un círculo en un context dado
 function dibujaCirculo(ctx, centerx, centery, radius ) {
    ctx.beginPath();
    ctx.arc(centerx, centery, radius, 0, 2 * Math.PI);
    ctx.stroke();
  }

  // Borar el context
  function clearContext(ctx, width, height) {
    ctx.clearRect (0, 0, width, height);
  }

});
