$(document).ready(function(){

  console.log("Welcome to Bubbles!");


  for (var i=0; i < 100; i++) {
    $("body").append('<div class="bubble"></div>');
  }

  $(".bubble").parent().css({position: 'relative'});

  $(".bubble").each(function() {
      var w = Math.floor((Math.random() * 100) + 1);
      var h = Math.floor((Math.random() * 100) + 1);

      var pt = Math.floor((Math.random() * 1000) + 1);
      var pl = Math.floor((Math.random() * 1000) + 1);

      $(this).width(w);
      $(this).height(h);

      $(this).css({top: pt, left: pl, position:'absolute'});

  });


});
