$(document).ready(function(){

  console.log("Welcome to jQuery.");


  var clicks = 0;

  $('.card').click(function(){
    $(this).toggleClass('back');
    $(this).find("span").css({display: "block"});

    if (clicks == 0) {
      clicks = 1;
    } else if (clicks == 1) {
      clicks = 2;
    } else if (clicks == 2) {
      clicks = 1;
    }

    if (clicks == 2) {
      setTimeout(hideUncompletedCards, 2000);
    }

  });

  function hideUncompletedCards() {
      $('.card').addClass("back");
      $('.card').find("span").css({display: "none"});
  }


});
