var cells = document.getElementsByClassName("cell");

[].forEach.call(cells, function (el) {
  el.addEventListener("click", whenClick);
  el.addEventListener("dblclick", whenDoubleClick);
});

document
.getElementById('reset')
.addEventListener("click", buttonClicked);

function buttonClicked() {
  var cells = document.getElementsByClassName("cell");

  [].forEach.call(cells, function (el) {
    el.innerHTML = "";
  });
}

function whenClick() {
  this.innerHTML = "X";
}

function whenDoubleClick() {
  this.innerHTML = "O";
}
