$(document).ready(function(){

  console.log("Welcome to jQuery.");

  $("#new-article").click(function(){

    var titulo = $("#form-title").val();
    var description = $("#form-description").val();
    var imagen = $("#form-image").val();

    var plantilla = "";
    plantilla += "<article>";
    plantilla += "<h2>";
    plantilla += titulo;
    plantilla += "</h2>";
    plantilla += "<p>";
    plantilla += description;
    plantilla += "</p>";

    // Diferentes formas de escribir comillas:
    // Forma 1
    plantilla += "<img src='";
    plantilla += imagen;
    plantilla += "'></img>";
    // Forma 2
    plantilla += '<img src="';
    plantilla += imagen;
    plantilla += '"></img>';
    // Forma 3
    plantilla += "<img src='" + imagen + "'></img>";
    // Forma 4
    plantilla += '<img src="' + imagen + '"></img>';
    // Forma 5
    plantilla += "<img src=\"" + imagen + "\"></img>";

    plantilla += "</article>";

    $("#content").append(plantilla);

    // Resetear los valores del formulario
    $("#form-title").val("");
    $("#form-description").val("");
    $("#form-image").val("");

    // Para evitar que la página se recargue;
    return false;

  });

});
